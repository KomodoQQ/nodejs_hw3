const Joi = require('joi');
const { Types } = require('mongoose');

const Truck = require('../models/truck');
const User = require('../models/user');

const truckSchema = Joi.object({
  type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT').required()
});

module.exports.getTrucks = (request, response) => {
  if (request.user.role !== 'DRIVER') {
    return response.status(403).json({ message: 'Forbidden user role for this request' });
  }
  Truck.find({ created_by: request.user._id })
    .exec()
    .then((trucks) => {
      response.status(200).json({ trucks });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};

module.exports.addTruck = (request, response) => {
  const { type } = request.body;
  if (!type) {
    return response.status(400).json({ message: 'Empty type of truck' });
  }
  User.findById(request.user._id)
    .exec()
    .then((user) => {
      if (!user) {
        return response.status(400).json({ message: 'User not found' });
      }
      if (user.role !== 'DRIVER') {
        return response.status(403).json({ message: 'Forbidden user role for this request' });
      }

      const truck = new Truck({
        created_by: request.user._id,
        assigned_to: null,
        type: type
      });
      truck
        .save()
        .then(() => {
          response.json({ message: 'Truck created successfully' });
        })
        .catch((err) => {
          response.status(500).json({ message: err.message });
        });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};

module.exports.getTruckById = (request, response) => {
  const { truckId } = request.params;
  if (request.user.role !== 'DRIVER') {
    return response.status(403).json({ message: 'Forbidden user role for this request' });
  }
  if (!Types.ObjectId.isValid(truckId)) {
    return response.status(400).json({ message: 'Invalid truck id' });
  }
  Truck.findById(truckId)
    .exec()
    .then((truck) => {
      if (!truck) {
        return response.status(404).json({ message: 'Truck not found' });
      }
      response.json({ truck });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};

module.exports.updateTruckById = (request, response) => {
  try {
    const { type } = Joi.attempt(request.body, truckSchema);
    const { truckId } = request.params;
    if (request.user.role !== 'DRIVER') {
      return response.status(403).json({ message: 'Forbidden user role for this request' });
    }
    if (!Types.ObjectId.isValid(truckId)) {
      return response.status(400).json({ message: 'Invalid truck id' });
    }
    Truck.findById(truckId)
      .exec()
      .then((truck) => {
        if (!truck) {
          return response.status(400).json({ message: 'Truck not found' });
        }
        if (truck.status === 'OL') {
          return response.status(400).json({ message: 'Truck is on load' });
        }
        Truck.findByIdAndUpdate(truckId, { type: type })
          .exec()
          .then(() => {
            response.json({ message: 'Truck details changed successfully' });
          })
          .catch((err) => {
            response.status(500).json({ message: err.message });
          });
      })
      .catch((err) => {
        response.status(500).json({ message: err.message });
      });
  } catch (err) {
    if (err.isJoi) {
      const message = err.details.map((e) => e.message).join('\n');
      response.status(400).json({ message: message });
    } else {
      response.status(500).json({ message: 'Server error' });
    }
  }
};

module.exports.deleteTruckById = (request, response) => {
  try {
    const { truckId } = request.params;
    if (request.user.role !== 'DRIVER') {
      return response.status(403).json({ message: 'Forbidden user role for this request' });
    }
    if (!Types.ObjectId.isValid(truckId)) {
      return response.status(400).json({ message: 'Invalid truck id' });
    }
    Truck.findById(truckId)
      .exec()
      .then((truck) => {
        if (!truck) {
          return response.status(404).json({ message: 'Truck not found' });
        }
        Truck.findByIdAndDelete(truckId)
          .exec()
          .then(() => {
            response.json({ message: 'Truck deleted successfully' });
          })
          .catch((err) => {
            response.status(500).json({ message: err.message });
          });
      })
      .catch((err) => {
        response.status(500).json({ message: err.message });
      });
  } catch (err) {
    response.status(500).json({ message: err.message });
  }
};

module.exports.assignTruckById = (request, response) => {
  const { truckId } = request.params;
  if (request.user.role !== 'DRIVER') {
    return response.status(403).json({ message: 'Forbidden user role for this request' });
  }
  if (!Types.ObjectId.isValid(truckId)) {
    return response.status(400).json({ message: 'Invalid truck id' });
  }
  Truck.find({ created_by: request.user._id, status: 'OL' })
    .exec()
    .then((trucks) => {
      if (trucks.length > 0) {
        return response.status(400).json({ message: 'Driver is on load already' });
      }
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
  Truck.findById(truckId)
    .exec()
    .then((truck) => {
      if (!truck) {
        return response.status(404).json({ message: 'Truck not found' });
      }
      if (truck.status == 'OL') {
        return response.status(400).json({ message: 'Truck is on load' });
      }
      if (truck.created_by != request.user._id) {
        return res.status(403).json({ message: 'Forbidden for this action' });
      }
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
  Truck.findOneAndUpdate({ status: 'IS' }, { assigned_to: undefined, status: 'OS' })
    .exec()
    .then((truck) => {
      if (!truck) {
        return response.status(404).json({ message: 'Truck info updated' });
      }
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
  Truck.findByIdAndUpdate(truckId, { assigned_to: request.user._id, status: 'IS' })
    .exec()
    .then((truck) => {
      if (!truck) {
        return response.status(404).json({ message: 'Truck for update not found' });
      }
      response.json({ message: 'Truck assigned successfully' });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};
