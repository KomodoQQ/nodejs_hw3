const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema(
  {
    created_by: {
      type: mongoose.Types.ObjectId,
      required: true
    },
    assigned_to: {
      type: mongoose.Types.ObjectId
    },
    status: {
      type: String,
      required: true,
      default: 'NEW',
      enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED']
    },
    state: {
      type: String,
      enum: ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery']
    },
    name: {
      type: String,
      required: true
    },
    payload: {
      type: Number,
      required: true
    },
    pickup_address: {
      type: String,
      required: true
    },
    delivery_address: {
      type: String,
      required: true
    },
    dimensions: {
      width: {
        type: Number,
        required: true
      },
      length: {
        type: Number,
        required: true
      },
      height: {
        type: Number,
        required: true
      }
    },
    logs: [
      {
        message: {
          type: String,
          required: true
        },
        time: {
          type: Date,
          required: true
        }
      }
    ],
    created_date: {
      type: Date,
      default: Date.now
    }
  },
  { versionKey: false }
);

module.exports = mongoose.model('load', loadSchema);
