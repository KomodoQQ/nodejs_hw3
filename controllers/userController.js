const User = require('../models/user');

module.exports.getUser = (request, response) => {
  User.findById(request.user._id)
    .exec()
    .then((user) => {
      if (!user) {
        return response.status(404).json({ message: 'User not found' });
      }
      const { _id, email, createdDate } = user;
      response.json({ user: { _id, email, createdDate } });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};

module.exports.deleteUser = (request, response) => {
  User.findByIdAndDelete(request.user._id)
    .exec()
    .then((user) => {
      if (!user) {
        return response.status(404).json({ message: 'User not found' });
      }
      response.json({ message: 'Profile deleted successfully' });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};

module.exports.changeUserPassword = (request, response) => {
  const { oldPassword, newPassword } = request.body;
  
  if (!oldPassword) {
    return response.status(400).json({
      message: 'No old password found'
    });
  }

  if (!newPassword) {
    return response.status(400).json({
      message: 'No new password found'
    });
  }

  if (newPassword === oldPassword) {
    return response.status(400).json({
      message: 'Old and new passwords are equal'
    });
  }
  User.findOneAndUpdate({_id: request.user._id, password: oldPassword}, { password: newPassword })
    .then((user) => {
      if (!user) {
        return response.status(400).json({ message: 'Wrong old password' });
      }
      response.json({ message: 'Password changed successfully' });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};