const express = require('express');
const mongoose = require('mongoose');
const app = express();

const { port } = require('./config/server');
const dbConfig = require('./config/database');

const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const loadRouter = require('./routers/loadRouter');
const truckRouter = require('./routers/truckRouter');

mongoose.connect(`mongodb+srv://${dbConfig.username}:${dbConfig.password}@${dbConfig.address}/${dbConfig.databaseName}?retryWrites=true&w=majority`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});

app.use(express.json());

app.use('/api', userRouter);
app.use('/api', authRouter);
app.use('/api', loadRouter);
app.use('/api', truckRouter);

app.listen(port, () => {
  console.log(`Server listens on ${port} port`);
});
