const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema(
  {
    created_by: {
      type: mongoose.Types.ObjectId,
      required: true
    },
    assigned_to: {
      type: mongoose.Types.ObjectId
    },
    status: {
      type: String,
      required: true,
      default: 'OS',
      enum: ['OL', 'IS', 'OS']
    },
    type: {
      type: String,
      required: true,
      enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT']
    },
    created_date: {
      type: Date,
      default: Date.now
    }
  },
  { versionKey: false }
);

module.exports = mongoose.model('truck', truckSchema);
