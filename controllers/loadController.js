const joi = require('joi');
const { Types } = require('mongoose');

const Load = require('../models/load');
const Truck = require('../models/truck');

const sprinterDimensions = {
  payload: 1700,
  height: 170,
  width: 300,
  length: 250
};

const smallStraightDimensions = {
  payload: 2500,
  height: 170,
  width: 500,
  length: 250
};

const largeStraightDimensions = {
  payload: 4000,
  height: 200,
  width: 700,
  length: 350
};

const addLoadSchema = joi.object({
  name: joi.string().required(),
  payload: joi.number().required().min(1).integer(),
  pickup_address: joi.string().required(),
  delivery_address: joi.string().required(),
  dimensions: joi.object({
    height: joi.number().integer().min(1),
    width: joi.number().integer().min(1),
    length: joi.number().integer().min(1)
  })
});

const updateLoadSchema = joi.object({
  name: joi.string(),
  payload: joi.number().min(1).integer(),
  pickup_address: joi.string(),
  delivery_address: joi.string(),
  dimensions: joi.object({
    height: joi.number().integer().min(1),
    width: joi.number().integer().min(1),
    length: joi.number().integer().min(1)
  })
});

const getLoadsSchema = joi.object({
  status: joi.string().valid('NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'),
  limit: joi.number().default(10).integer().min(1).max(50),
  offset: joi.number().default(0).integer()
});

module.exports.getLoads = (request, response) => {
  try {
    const { status, limit, offset } = joi.attempt(request.query, getLoadsSchema);
    if (request.user.role == 'SHIPPER') {
      Load.find({ created_by: request.user._id, status: status || 'NEW' }, null, { skip: offset, limit: limit })
        .exec()
        .then((loads) => {
          response.json({ loads });
        })
        .catch((err) => {
          response.status(500).json({ message: err.message });
        });
    }
    if (request.user.role == 'DRIVER') {
      Load.find({ assigned_to: request.user._id, status: status || 'NEW' }, null, { skip: offset, limit: limit })
        .exec()
        .then((loads) => {
          response.json({ loads });
        })
        .catch((err) => {
          response.status(500).json({ message: err.message });
        });
    }
  } catch (err) {
    if (err.isJoi) {
      const message = err.details.map((e) => e.message).join('\n');
      response.status(400).json({ message: message });
    } else {
      response.status(500).json({ message: err.message });
    }
  }
};

module.exports.addLoad = (request, response) => {
  try {
    const { name, payload, pickup_address, delivery_address, dimensions } = joi.attempt(request.body, addLoadSchema);

    if (request.user.role != 'SHIPPER') {
      return response.status(403).json({ message: 'Forbidden user role for this request' });
    }

    const load = new Load({
      created_by: request.user._id,
      name: name,
      payload: payload,
      pickup_address: pickup_address,
      delivery_address: delivery_address,
      dimensions: dimensions
    });
    load
      .save()
      .then(() => {
        response.json({ message: 'Load created successfully' });
      })
      .catch((err) => {
        response.status(500).json({ message: err.message });
      });
  } catch (err) {
    if (err.isJoi) {
      const message = err.details.map((e) => e.message).join('\n');
      response.status(400).json({ message: message });
    } else {
      response.status(500).json({ message: err.message });
    }
  }
};

module.exports.getActiveLoads = (request, response) => {
  try {
    if (request.user.role != 'DRIVER') {
      return response.status(403).json({ message: 'Forbidden user role for this request' });
    }
    Load.findOne({ status: 'ASSIGNED', assigned_to: request.user._id })
      .exec()
      .then((load) => {
        if (!load) {
          return response.status(400).json({ message: 'Assigned loads not found' });
        }
        response.json({ load });
      })
      .catch((err) => {
        response.status(500).json({ message: err.message });
      });
  } catch (error) {
    response.status(500).json({ message: err.message });
  }
};

module.exports.updateLoadState = (request, response) => {
  try {
    if (request.user.role != 'DRIVER') {
      return response.status(403).json({ message: 'Forbidden user role for this request' });
    }
    Load.findOne({ assigned_to: request.user._id, status: 'ASSIGNED' })
      .exec()
      .then((load) => {
        if (!load) {
          return response.status(404).json({ message: 'Load not found' });
        }
        if (load.state == '') {
          Load.findOneAndUpdate(
            { _id: load._id },
            {
              state: 'En route to Pick Up',
              $push: {
                logs: {
                  message: "Load state changed to 'En route to Pick Up'",
                  time: Date.now()
                }
              }
            }
          )
            .exec()
            .then(() => {
              return response.json({ message: `Load state changed to 'En route to Pick Up'` });
            })
            .catch((err) => {
              return response.status(500).json({ message: err.message });
            });
        }
        if (load.state == 'En route to Pick Up') {
          Load.findOneAndUpdate(
            { _id: load._id },
            {
              state: 'Arrived to Pick Up',
              $push: {
                logs: {
                  message: "Load state changed to 'Arrived to Pick Up'",
                  time: Date.now()
                }
              }
            }
          )
            .exec()
            .then(() => {
              return response.json({ message: `Load state changed to 'Arrived to Pick Up'` });
            })
            .catch((err) => {
              return response.status(500).json({ message: err.message });
            });
        } else if (load.state == 'Arrived to Pick Up') {
          Load.findOneAndUpdate(
            { _id: load._id },
            {
              state: 'En route to delivery',
              $push: {
                logs: {
                  message: "Load state changed to 'En route to delivery'",
                  time: Date.now()
                }
              }
            }
          )
            .exec()
            .then(() => {
              return response.json({ message: `Load state changed to 'En route to delivery'` });
            })
            .catch((err) => {
              return response.status(500).json({ message: err.message });
            });
        } else if (load.state == 'En route to delivery') {
          Load.findOneAndUpdate(
            { _id: load._id },
            {
              state: 'Arrived to delivery',
              status: 'SHIPPED',
              $push: {
                logs: {
                  message: "Load state changed to 'Arrived to delivery'",
                  time: Date.now()
                }
              }
            }
          )
            .exec()
            .then(() => {
              Truck.findOneAndUpdate({ status: 'OL', assigned_to: load.assigned_to }, { status: 'IS' })
                .exec()
                .then(() => {})
                .catch((err) => {
                  return response.status(500).json({ message: err.message });
                });
              return response.json({ message: `Load state changed to 'Arrived to delivery'` });
            })
            .catch((err) => {
              return response.status(500).json({ message: err.message });
            });
        }
      })
      .catch((err) => {
        response.status(500).json({ message: err.message });
      });
  } catch (err) {
    response.status(500).json({ message: err.message });
  }
};

module.exports.getUserLoad = (request, response) => {
  try {
    const { id } = request.params;
    let searchParams = {};
    if (!Types.ObjectId.isValid(id)) {
      return response.status(400).json({ message: 'invalid load id' });
    }
    if (request.user.role == 'DRIVER') {
      searchParams._id = id;
      searchParams.assigned_to = request.user._id;
    }
    if (request.user.role == 'SHIPPER') {
      searchParams._id = id;
      searchParams.created_by = request.user._id;
    }
    Load.findOne(searchParams)
      .exec()
      .then((load) => {
        if (!load) {
          return response.status(404).json({ message: 'Load for this user with this id not found' });
        }
        return response.json({ load });
      })
      .catch((err) => {
        return response.status(500).json({ message: err.message });
      });
  } catch (err) {
    return response.status(500).json({ message: err.message });
  }
};

module.exports.updateUserLoad = (request, response) => {
  try {
    const { id } = request.params;
    const { name, payload, pickup_address, delivery_address, dimensions } = joi.attempt(request.body, updateLoadSchema);
    if (request.user.role != 'SHIPPER') {
      return response.status(403).json({ message: 'Forbidden user role for this request' });
    }
    if (!Types.ObjectId.isValid(id)) {
      return response.status(400).json({ message: 'Invalid load id' });
    }
    Load.findOne({ _id: id, created_by: request.user._id })
      .exec()
      .then((load) => {
        if (!load) {
          return response.status(404).json({ message: 'Load not found' });
        }
        if (load.status != 'NEW') {
          return response.status(404).json({ message: 'This load can not be updated' });
        }
        Load.findByIdAndUpdate(
          load._id,
          {
            name: name,
            payload: payload,
            pickup_address: pickup_address,
            delivery_address: delivery_address,
            dimensions: dimensions
          },
          { omitUndefined: true }
        )
          .exec()
          .then(() => {
            return response.json({ message: 'Load details changed successfully' });
          })
          .catch((err) => {
            return response.status(500).json({ message: err.message });
          });
      })
      .catch((err) => {
        return response.status(500).json({ message: err.message });
      });
  } catch (err) {
    if (err.isJoi) {
      const message = err.details.map((e) => e.message).join('\n');
      response.status(400).json({ message: message });
    } else {
      response.status(500).json({ message: 'Server error' });
    }
  }
};

module.exports.deleteUserLoad = (request, response) => {
  try {
    const { id } = request.params;
    if (request.user.role != 'SHIPPER') {
      return response.status(403).json({ message: 'Forbidden user role for this request' });
    }
    if (!Types.ObjectId.isValid(id)) {
      return response.status(400).json({ message: 'Invalid load id' });
    }
    Load.findById(id)
      .exec()
      .then((load) => {
        if (!load) {
          return response.status(400).json({ message: 'Load not found' });
        }
        if (load.status != 'NEW') {
          return response.status(400).json({ message: 'Load is already taken and can not be deleted' });
        }
        Load.findByIdAndDelete(load._id)
          .exec()
          .then(() => {
            response.json({ message: 'Load deleted successfully' });
          })
          .catch((err) => {
            return response.status(500).json({ message: err.message });
          });
      })
      .catch((err) => {
        return response.status(500).json({ message: err.message });
      });
  } catch (err) {
    response.status(500).json({ message: err.message });
  }
};

module.exports.postUserLoad = (request, response) => {
  try {
    const { id } = request.params;
    if (request.user.role != 'SHIPPER') {
      return response.status(403).json({ message: 'Forbidden user role for this request' });
    }
    if (!Types.ObjectId.isValid(id)) {
      return response.status(400).json({ message: 'Invalid load id' });
    }
    Load.findById(id)
      .exec()
      .then((load) => {
        if (!load) {
          return response.status(404).json({ message: 'Load not found' });
        }
        if (load.status == 'NEW') {
          Load.findByIdAndUpdate(load._id, {
            status: 'POSTED',
            $push: {
              logs: {
                message: `Load posted`,
                time: Date.now()
              }
            }
          })
            .exec()
            .then(() => {})
            .catch((err) => {
              return response.status(500).json({ message: err.message });
            });
        }

        Truck.find({ status: 'IS' })
          .exec()
          .then((trucks) => {
            if (trucks.length == 0) {
              return response.status(400).json({ message: 'No trucks available' });
            }
            for (currTruck of trucks) {
              if (currTruck.type == 'SPRINTER') {
                if (isTruckSizeValid(load.dimensions, load.payload, sprinterDimensions)) {
                  Truck.findByIdAndUpdate({ _id: currTruck._id }, { status: 'OL' })
                    .exec()
                    .then(() => {
                      Load.findByIdAndUpdate(load._id, {
                        status: 'ASSIGNED',
                        state: 'En route to Pick Up',
                        assigned_to: currTruck.assigned_to,
                        $push: {
                          logs: {
                            message: `Load assigned to driver with id ${currTruck.assigned_to}`,
                            time: Date.now()
                          }
                        }
                      })
                        .exec()
                        .then(() => {
                          return response.json({ message: 'Load posted successfully', driver_found: true });
                        })
                        .catch((err) => {
                          return response.status(500).json({ message: err.message });
                        });
                    })
                    .catch((err) => {
                      return response.status(500).json({ message: err.message });
                    });
                }
              } else if (currTruck.type == 'SMALL STRAIGHT') {
                if (isTruckSizeValid(load.dimensions, load.payload, smallStraightDimensions)) {
                  Truck.findByIdAndUpdate({ _id: currTruck._id }, { status: 'OL' })
                    .exec()
                    .then(() => {
                      Load.findByIdAndUpdate(load._id, {
                        status: 'ASSIGNED',
                        state: 'En route to Pick Up',
                        assigned_to: currTruck.assigned_to,
                        $push: {
                          logs: {
                            message: `Load assigned to driver with id ${currTruck.assigned_to}`,
                            time: Date.now()
                          }
                        }
                      })
                        .exec()
                        .then(() => {
                          return response.json({ message: 'Load posted successfully', driver_found: true });
                        })
                        .catch((err) => {
                          return response.status(500).json({ message: err.message });
                        });
                    })
                    .catch((err) => {
                      return response.status(500).json({ message: err.message });
                    });
                }
              } else if (currTruck.type == 'LARGE STRAIGHT') {
                if (isTruckSizeValid(load.dimensions, load.payload, largeStraightDimensions)) {
                  Truck.findByIdAndUpdate({ _id: currTruck._id }, { status: 'OL' })
                    .exec()
                    .then(() => {
                      Load.findByIdAndUpdate(load._id, {
                        status: 'ASSIGNED',
                        state: 'En route to Pick Up',
                        assigned_to: currTruck.assigned_to,
                        $push: {
                          logs: {
                            message: `Load assigned to driver with id ${currTruck.assigned_to}`,
                            time: Date.now()
                          }
                        }
                      })
                        .exec()
                        .then(() => {
                          return response.json({ message: 'Load posted successfully', driver_found: true });
                        })
                        .catch((err) => {
                          return response.status(500).json({ message: err.message });
                        });
                    })
                    .catch((err) => {
                      return response.status(500).json({ message: err.message });
                    });
                }
              } else {
                return response.status(400).json({ message: 'Wrong truck type' });
              }
            }
          })
          .catch((err) => {
            response.status(500).json({ message: err.message });
          });
      })
      .catch((err) => {
        response.status(500).json({ message: err.message });
      });
  } catch (err) {
    response.status(500).json({ message: err.message });
  }
};

module.exports.getShippingInfo = (request, response) => {
  try {
    const { id } = request.params;
    if (request.user.role != 'SHIPPER') {
      return response.status(403).json({ message: 'Forbidden user role for this request' });
    }
    if (!Types.ObjectId.isValid(id)) {
      return response.status(400).json({ message: 'invalid load id' });
    }
    Load.findById(id)
      .exec()
      .then((load) => {
        if (!load) {
          return response.status(404).json({ message: 'Load not found' });
        }
        if (load.status == 'NEW') {
          return response.status(400).json({ message: 'No shipment for this load' });
        }
        Truck.findOne({ assigned_to: load.assigned_to })
          .exec()
          .then((truck) => {
            if (!truck) {
              return response.status(404).json({ message: 'Truck not found' });
            }
            return response.json({ load, truck });
          })
          .catch((err) => {
            return response.status(500).json({ message: err.message });
          });
      })
      .catch((err) => {
        return response.status(500).json({ message: err.message });
      });
  } catch (err) {
    return response.status(500).json({ message: err.message });
  }
};

const isTruckSizeValid = (loadSize, loadPayload, truckSize) => {
  if (
    truckSize.payload > loadPayload &&
    truckSize.height > loadSize.height &&
    truckSize.width > loadSize.width &&
    truckSize.length > loadSize.length
  ) {
    return true;
  } else {
    return false;
  }
};
