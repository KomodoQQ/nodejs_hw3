const jwt = require('jsonwebtoken');
const User = require('../models/user');

const { secret } = require('../config/auth');

module.exports.register = (request, response) => {
  const { email, password, role } = request.body;

  const user = new User({ email, password, role });
  user
    .save()
    .then(() => {
      response.json({ status: 'Profile created successfully' });
    })
    .catch((err) => {
      response.status(500).json({ status: err.message });
    });
};

module.exports.login = (request, response) => {
  const { email, password } = request.body;

  User.findOne({ email, password })
    .exec()
    .then((user) => {
      if (!user) {
        return response.status(400).json({ status: 'No user with such username and password found' });
      }
      response.json({ status: 'success', token: jwt.sign(JSON.stringify(user), secret) });
    })
    .catch((err) => {
      response.status(500).json({ status: err.message });
    });
};
