const express = require('express');
const router = express.Router();

const authMiddleware = require('../middlewares/authMiddleware');

const {
  getTrucks,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById
} = require('../controllers/truckController');

router.get('/trucks', authMiddleware, getTrucks);
router.post('/trucks', authMiddleware, addTruck);
router.get('/trucks/:truckId', authMiddleware, getTruckById);
router.put('/trucks/:truckId', authMiddleware, updateTruckById);
router.delete('/trucks/:truckId', authMiddleware, deleteTruckById);
router.post('/trucks/:truckId/assign', authMiddleware, assignTruckById);

module.exports = router;
