const express = require('express');
const router = express.Router();

const authMiddleware = require('../middlewares/authMiddleware');

const {
  getLoads,
  addLoad,
  getActiveLoads,
  updateLoadState,
  getUserLoad,
  updateUserLoad,
  deleteUserLoad,
  postUserLoad,
  getShippingInfo
} = require('../controllers/loadController');

router.get('/loads', authMiddleware, getLoads);
router.post('/loads', authMiddleware, addLoad);

router.get('/loads/active', authMiddleware, getActiveLoads);
router.patch('/loads/active/state', authMiddleware, updateLoadState);

router.get('/loads/:id', authMiddleware, getUserLoad);
router.put('/loads/:id', authMiddleware, updateUserLoad);
router.delete('/loads/:id', authMiddleware, deleteUserLoad);
router.post('/loads/:id/post', authMiddleware, postUserLoad);
router.get('/loads/:id/shipping_info', authMiddleware, getShippingInfo);

module.exports = router;
