const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  createdDate: {
    type: Date,
    default: Date.now
  },
  role: {
    type: String,
    required: true,
    enum: ['SHIPPER', 'DRIVER']
  }
}, {versionKey: false});

module.exports = mongoose.model('user', userSchema);
